import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import { Auth } from "aws-amplify";
import Home from "../views/Home.vue";
import Inventory from "../views/Inventory.vue";
import SignIn from "../views/SignIn.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/inventory",
    name: "Inventory",
    component: Inventory,
  },
  {
    path: "/sign-in",
    name: "Sign In",
    component: SignIn,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  const redirect = {} as any;

  try {
    await Auth.currentAuthenticatedUser();

    if (to.name === "Sign In") {
      redirect.name = "Home";
    }
  } catch (e) {
    if (to.name !== "Sign In") {
      redirect.name = "Sign In";
    }
  } finally {
    next(redirect);
  }
});

export default router;
