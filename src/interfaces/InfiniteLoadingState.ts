export default interface InfiniteLoadingState {
  complete: () => void;
  loaded: () => void;
  reset: () => void;
  error: () => void;
}
