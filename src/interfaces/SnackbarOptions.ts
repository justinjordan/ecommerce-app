interface SnackbarAction {
  label: string;
  callback: () => void;
}

export default interface SnackbarOptions {
  message: string;
  action?: SnackbarAction;
  timeout?: number;
  timeoutAction?: () => void;
}
