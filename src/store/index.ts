import Vue from "vue";
import Vuex, { ModuleTree } from "vuex";
import SnackbarOptions from "@/interfaces/SnackbarOptions";
import auth from "./modules/auth";
import products from "./modules/products";
import inventory from "./modules/inventory";

Vue.use(Vuex);

type RootState = {
  snackbar: boolean;
  snackbarOptions: SnackbarOptions;
};

const modules: ModuleTree<RootState> = {
  auth,
  products,
  inventory,
};

export default new Vuex.Store({
  state: {
    snackbar: false,
    snackbarOptions: {
      message: "",
    },
  },
  mutations: {
    resetSnackbar(state) {
      state.snackbar = false;
      state.snackbarOptions = {
        message: "",
      };
    },
    setSnackbar(state, options: SnackbarOptions) {
      state.snackbar = true;
      state.snackbarOptions = options;
    },
  },
  actions: {
    createSnackbar({ commit }, options: SnackbarOptions) {
      commit("setSnackbar", options);
    },
  },
  modules,
});
