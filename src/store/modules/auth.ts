import { Module } from "vuex";
import { Auth } from "aws-amplify";
import router from "@/router";

const initialState = {
  //
};

const authModule: Module<any, any> = {
  namespaced: true,
  state: {
    ...initialState,
  },
  mutations: {
    reset(state) {
      Object.assign(state, initialState);
    },
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    async getUser() {
      const user = await Auth.currentUserInfo();

      if (!user) {
        throw new Error("Access Denied");
      }

      return user;
    },
    async signOut({ commit }) {
      await Auth.signOut();
      commit("setUser", null);
      router.push("/sign-in");
    },
    async signIn({ commit }, { email, password }) {
      const user = await Auth.signIn(email, password);

      if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
        await Auth.completeNewPassword(user, password);
      }

      commit("setUser", user);
    },
  },
};

export default authModule;
