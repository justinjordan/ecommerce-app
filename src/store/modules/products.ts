import { Module } from "vuex";
import { API, graphqlOperation } from "aws-amplify";
import * as queries from "@/graphql/queries";
import {
  ListProductsQuery,
  ListProductsQueryVariables,
  ProductsByOwnerQuery,
  ProductsByOwnerQueryVariables,
} from "@/API";

const initialState = {
  //
};

const productsModule: Module<any, any> = {
  namespaced: true,
  state: {
    ...initialState,
  },
  mutations: {
    reset(state) {
      Object.assign(state, initialState);
    },
  },
  actions: {
    async productsByOwner(context, variables: ProductsByOwnerQueryVariables) {
      const response = (await API.graphql(
        graphqlOperation(queries.productsByOwner, variables)
      )) as { data: ProductsByOwnerQuery };

      return response?.data;
    },
    async listProducts(context, variables: ListProductsQueryVariables) {
      const response = (await API.graphql(
        graphqlOperation(queries.listProducts, variables)
      )) as { data: ListProductsQuery };

      return response?.data;
    },
  },
};

export default productsModule;
