import { Module } from "vuex";
import { API, graphqlOperation } from "aws-amplify";
import * as queries from "@/graphql/queries";
import { InventoryByOwnerQuery, InventoryByOwnerQueryVariables } from "@/API";

const initialState = {
  //
};

const inventoryModule: Module<any, any> = {
  namespaced: true,
  state: {
    ...initialState,
  },
  mutations: {
    reset(state) {
      Object.assign(state, initialState);
    },
  },
  actions: {
    async getInventoryByOwner(
      context,
      variables: InventoryByOwnerQueryVariables
    ) {
      const response = (await API.graphql(
        graphqlOperation(queries.inventoryByOwner, variables)
      )) as { data: InventoryByOwnerQuery };

      return response?.data;
    },
  },
};

export default inventoryModule;
