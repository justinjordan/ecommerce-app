import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Amplify from "aws-amplify";
import aws_exports from "./aws-exports";
import Infinite from "@/components/Infinite.vue";

Amplify.configure(aws_exports);

Vue.config.productionTip = false;

Vue.component("Infinite", Infinite);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
