/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser($owner: String) {
    onCreateUser(owner: $owner) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser($owner: String) {
    onUpdateUser(owner: $owner) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser($owner: String) {
    onDeleteUser(owner: $owner) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const onCreateProduct = /* GraphQL */ `
  subscription OnCreateProduct($owner: String) {
    onCreateProduct(owner: $owner) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const onUpdateProduct = /* GraphQL */ `
  subscription OnUpdateProduct($owner: String) {
    onUpdateProduct(owner: $owner) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const onDeleteProduct = /* GraphQL */ `
  subscription OnDeleteProduct($owner: String) {
    onDeleteProduct(owner: $owner) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const onCreateInventory = /* GraphQL */ `
  subscription OnCreateInventory($owner: String) {
    onCreateInventory(owner: $owner) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateInventory = /* GraphQL */ `
  subscription OnUpdateInventory($owner: String) {
    onUpdateInventory(owner: $owner) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteInventory = /* GraphQL */ `
  subscription OnDeleteInventory($owner: String) {
    onDeleteInventory(owner: $owner) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const onCreateOrder = /* GraphQL */ `
  subscription OnCreateOrder($owner: String) {
    onCreateOrder(owner: $owner) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
export const onUpdateOrder = /* GraphQL */ `
  subscription OnUpdateOrder($owner: String) {
    onUpdateOrder(owner: $owner) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
export const onDeleteOrder = /* GraphQL */ `
  subscription OnDeleteOrder($owner: String) {
    onDeleteOrder(owner: $owner) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
