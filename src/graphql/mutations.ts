/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const createProduct = /* GraphQL */ `
  mutation CreateProduct(
    $input: CreateProductInput!
    $condition: ModelProductConditionInput
  ) {
    createProduct(input: $input, condition: $condition) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const updateProduct = /* GraphQL */ `
  mutation UpdateProduct(
    $input: UpdateProductInput!
    $condition: ModelProductConditionInput
  ) {
    updateProduct(input: $input, condition: $condition) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const deleteProduct = /* GraphQL */ `
  mutation DeleteProduct(
    $input: DeleteProductInput!
    $condition: ModelProductConditionInput
  ) {
    deleteProduct(input: $input, condition: $condition) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const createInventory = /* GraphQL */ `
  mutation CreateInventory(
    $input: CreateInventoryInput!
    $condition: ModelInventoryConditionInput
  ) {
    createInventory(input: $input, condition: $condition) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const updateInventory = /* GraphQL */ `
  mutation UpdateInventory(
    $input: UpdateInventoryInput!
    $condition: ModelInventoryConditionInput
  ) {
    updateInventory(input: $input, condition: $condition) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const deleteInventory = /* GraphQL */ `
  mutation DeleteInventory(
    $input: DeleteInventoryInput!
    $condition: ModelInventoryConditionInput
  ) {
    deleteInventory(input: $input, condition: $condition) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const createOrder = /* GraphQL */ `
  mutation CreateOrder(
    $input: CreateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    createOrder(input: $input, condition: $condition) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
export const updateOrder = /* GraphQL */ `
  mutation UpdateOrder(
    $input: UpdateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    updateOrder(input: $input, condition: $condition) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
export const deleteOrder = /* GraphQL */ `
  mutation DeleteOrder(
    $input: DeleteOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    deleteOrder(input: $input, condition: $condition) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
