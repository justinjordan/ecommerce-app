/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      email
      superadmin
      shopName
      rememberToken
      createdAt
      updatedAt
      cardBrand
      cardLastFour
      trialEndsAt
      shopDomain
      isEnabled
      billingPlan
      trialStartsAt
      owner
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        email
        superadmin
        shopName
        rememberToken
        createdAt
        updatedAt
        cardBrand
        cardLastFour
        trialEndsAt
        shopDomain
        isEnabled
        billingPlan
        trialStartsAt
        owner
      }
      nextToken
    }
  }
`;
export const getProduct = /* GraphQL */ `
  query GetProduct($id: ID!) {
    getProduct(id: $id) {
      id
      productId
      productName
      description
      style
      brand
      createdAt
      updatedAt
      url
      productType
      shippingPrice
      note
      adminId
      inventory {
        items {
          id
          productId
          quantity
          color
          size
          weight
          priceCents
          salePriceCents
          costCents
          sku
          length
          width
          height
          note
          owner
          createdAt
          updatedAt
        }
        nextToken
      }
      orders {
        items {
          id
          productId
          streetAddress
          apartment
          city
          state
          countryCode
          zip
          phoneNumber
          email
          name
          orderStatus
          paymentRef
          transactionId
          paymentAmtCents
          shipChargedCents
          subtotalCents
          totalCents
          shipperName
          paymentDate
          shippedDate
          trackingNumber
          taxTotalCents
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      owner
    }
  }
`;
export const listProducts = /* GraphQL */ `
  query ListProducts(
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProducts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      nextToken
    }
  }
`;
export const getInventory = /* GraphQL */ `
  query GetInventory($id: ID!) {
    getInventory(id: $id) {
      id
      productId
      quantity
      color
      size
      weight
      priceCents
      salePriceCents
      costCents
      sku
      length
      width
      height
      note
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
      createdAt
      updatedAt
    }
  }
`;
export const listInventories = /* GraphQL */ `
  query ListInventories(
    $filter: ModelInventoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listInventories(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        productId
        quantity
        color
        size
        weight
        priceCents
        salePriceCents
        costCents
        sku
        length
        width
        height
        note
        product {
          id
          productId
          productName
          description
          style
          brand
          createdAt
          updatedAt
          url
          productType
          shippingPrice
          note
          adminId
          owner
        }
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getOrder = /* GraphQL */ `
  query GetOrder($id: ID!) {
    getOrder(id: $id) {
      id
      productId
      streetAddress
      apartment
      city
      state
      countryCode
      zip
      phoneNumber
      email
      name
      orderStatus
      paymentRef
      transactionId
      paymentAmtCents
      shipChargedCents
      subtotalCents
      totalCents
      shipperName
      paymentDate
      shippedDate
      trackingNumber
      taxTotalCents
      createdAt
      updatedAt
      product {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      owner
    }
  }
`;
export const listOrders = /* GraphQL */ `
  query ListOrders(
    $filter: ModelOrderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listOrders(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        productId
        streetAddress
        apartment
        city
        state
        countryCode
        zip
        phoneNumber
        email
        name
        orderStatus
        paymentRef
        transactionId
        paymentAmtCents
        shipChargedCents
        subtotalCents
        totalCents
        shipperName
        paymentDate
        shippedDate
        trackingNumber
        taxTotalCents
        createdAt
        updatedAt
        product {
          id
          productId
          productName
          description
          style
          brand
          createdAt
          updatedAt
          url
          productType
          shippingPrice
          note
          adminId
          owner
        }
        owner
      }
      nextToken
    }
  }
`;
export const productsByOwner = /* GraphQL */ `
  query ProductsByOwner(
    $owner: String
    $sortDirection: ModelSortDirection
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
  ) {
    productsByOwner(
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      nextToken
    }
  }
`;
export const productsByDate = /* GraphQL */ `
  query ProductsByDate(
    $createdAt: AWSDateTime
    $sortDirection: ModelSortDirection
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
  ) {
    productsByDate(
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        productId
        productName
        description
        style
        brand
        createdAt
        updatedAt
        url
        productType
        shippingPrice
        note
        adminId
        inventory {
          nextToken
        }
        orders {
          nextToken
        }
        owner
      }
      nextToken
    }
  }
`;
export const inventoryByOwner = /* GraphQL */ `
  query InventoryByOwner(
    $owner: String
    $sortDirection: ModelSortDirection
    $filter: ModelInventoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    inventoryByOwner(
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        productId
        quantity
        color
        size
        weight
        priceCents
        salePriceCents
        costCents
        sku
        length
        width
        height
        note
        product {
          id
          productId
          productName
          description
          style
          brand
          createdAt
          updatedAt
          url
          productType
          shippingPrice
          note
          adminId
          owner
        }
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const ordersByOwner = /* GraphQL */ `
  query OrdersByOwner(
    $owner: String
    $sortDirection: ModelSortDirection
    $filter: ModelOrderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    ordersByOwner(
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        productId
        streetAddress
        apartment
        city
        state
        countryCode
        zip
        phoneNumber
        email
        name
        orderStatus
        paymentRef
        transactionId
        paymentAmtCents
        shipChargedCents
        subtotalCents
        totalCents
        shipperName
        paymentDate
        shippedDate
        trackingNumber
        taxTotalCents
        createdAt
        updatedAt
        product {
          id
          productId
          productName
          description
          style
          brand
          createdAt
          updatedAt
          url
          productType
          shippingPrice
          note
          adminId
          owner
        }
        owner
      }
      nextToken
    }
  }
`;
