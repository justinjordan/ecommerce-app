# ecommerce-app

## Amplify Setup

```
npm i -g @amplify/cli
amplify configure
```

## Clone and Connect AWS

```
mkdir ecommerce-app
cd ecommerce-app
amplify init --app https://gitlab.com/justinjordan/ecommerce-app
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
