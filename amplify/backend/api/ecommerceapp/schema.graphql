type User
  @model
  @auth(rules: [{ allow: owner }, { allow: groups, groups: ["superadmin"] }]) {
  id: ID!
  name: String
  email: String
  superadmin: Boolean
  shopName: String
  rememberToken: String
  createdAt: AWSDateTime
  updatedAt: AWSDateTime
  cardBrand: String
  cardLastFour: String
  trialEndsAt: AWSDateTime
  shopDomain: String
  isEnabled: Boolean
  billingPlan: String
  trialStartsAt: AWSDateTime
  owner: String
}

type Product
  @model
  @key(
    name: "productsByOwner"
    fields: ["owner"]
    queryField: "productsByOwner"
  )
  @key(
    name: "productsByDate"
    fields: ["createdAt"]
    queryField: "productsByDate"
  )
  @auth(rules: [{ allow: owner }, { allow: groups, groups: ["superadmin"] }]) {
  id: ID!
  productId: Int
  productName: String!
  description: String
  style: String
  brand: String
  createdAt: AWSDateTime
  updatedAt: AWSDateTime
  url: String
  productType: String
  shippingPrice: Int
  note: String
  adminId: ID
  inventory: [Inventory] @connection(name: "ProductInventory")
  orders: [Order] @connection(name: "ProductOrders")
  owner: String
}

type Inventory
  @model
  @key(
    name: "inventoryByOwner"
    fields: ["owner"]
    queryField: "inventoryByOwner"
  )
  @auth(rules: [{ allow: owner }, { allow: groups, groups: ["superadmin"] }]) {
  id: ID!
  productId: ID!
  quantity: Int!
  color: String
  size: String
  weight: Float
  priceCents: Int!
  salePriceCents: Int!
  costCents: Int!
  sku: String!
  length: Float
  width: Float
  height: Float
  note: String
  product: Product @connection(name: "ProductInventory")
  owner: String
}

type Order
  @model
  @key(name: "ordersByOwner", fields: ["owner"], queryField: "ordersByOwner")
  @auth(rules: [{ allow: owner }, { allow: groups, groups: ["superadmin"] }]) {
  id: ID!
  productId: ID!
  streetAddress: String
  apartment: String
  city: String
  state: String
  countryCode: String
  zip: String
  phoneNumber: String
  email: String
  name: String
  orderStatus: String
  paymentRef: String
  transactionId: String
  paymentAmtCents: Int
  shipChargedCents: Int
  subtotalCents: Int
  totalCents: Int
  shipperName: String
  paymentDate: AWSDateTime
  shippedDate: AWSDateTime
  trackingNumber: String
  taxTotalCents: Int
  createdAt: AWSDateTime
  updatedAt: AWSDateTime
  product: Product @connection(name: "ProductOrders")
  owner: String
}
