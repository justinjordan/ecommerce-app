const uuidv5 = require("uuid").v5;
const csv = require("csvtojson");
const path = require("path");
const { chunkArray, AWSDateTime } = require("../utils");

// for seeding UUID generation
const { userNamespace, productNamespace } = require("../uuid-namespaces");

module.exports = async function ({
  asyncBatchWriteItem,
  usernames,
  productTable,
}) {
  const productOwners = {};
  const productsCsv = await csv().fromFile(
    path.join(__dirname, "../fixtures/products.csv")
  );

  // chunk into queries of 25 (DynamoDB limit)
  const chunkedProductsCsv = chunkArray(productsCsv, 25);

  try {
    for (let products of chunkedProductsCsv) {
      const params = {
        RequestItems: {
          [productTable]: products
            .map((product) => {
              const id = uuidv5(product.id, productNamespace);
              const adminUUID = uuidv5(product.admin_id, userNamespace);
              const owner = usernames[adminUUID];
              productOwners[id] = owner;

              return !owner
                ? null
                : {
                    PutRequest: {
                      Item: {
                        __typename: { S: "Product" }, // graphql model name
                        owner: { S: owner }, // admin cognito username
                        id: { S: id },
                        productId: { S: product.id },
                        productName: { S: String(product.product_name) },
                        description: { S: String(product.description) },
                        style: { S: String(product.style) },
                        brand: { S: String(product.brand) },
                        createdAt: { S: AWSDateTime(product.created_at) },
                        updatedAt: { S: AWSDateTime(product.updated_at) },
                        url: { S: String(product.url) },
                        productType: { S: String(product.product_type) },
                        shippingPrice: { N: String(product.shipping_price) },
                        note: { S: String(product.note) },
                        adminId: { S: adminUUID },
                      },
                    },
                  };
            })
            .filter((v) => v),
        },
      };

      await asyncBatchWriteItem(params);
    }

    console.log("Products seeded!");
  } catch (e) {
    console.error("Failed to seed products: ", e.message);
  }

  return {
    productOwners,
  };
};
