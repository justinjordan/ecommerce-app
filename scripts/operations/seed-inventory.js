const uuidv5 = require("uuid").v5;
const csv = require("csvtojson");
const path = require("path");
const { chunkArray, AWSDateTime } = require("../utils");

// for seeding UUID generation
const { inventoryNamespace, productNamespace } = require("../uuid-namespaces");

module.exports = async function ({
  asyncBatchWriteItem,
  inventoryTable,
  productOwners,
}) {
  const inventoryCsv = await csv().fromFile(
    path.join(__dirname, "../fixtures/inventory.csv")
  );

  // chunk into queries of 25 (DynamoDB limit)
  const chunkedInventoryCsv = chunkArray(inventoryCsv, 25);

  try {
    for (let inventory of chunkedInventoryCsv) {
      const params = {
        RequestItems: {
          [inventoryTable]: inventory
            .map((item) => {
              const id = uuidv5(item.id, inventoryNamespace);
              const productId = uuidv5(item.product_id, productNamespace);
              const owner = productOwners[productId];

              return {
                PutRequest: {
                  Item: {
                    __typename: { S: "Inventory" }, // graphql model name
                    createdAt: { S: AWSDateTime(new Date()) },
                    updatedAt: { S: AWSDateTime(new Date()) },
                    owner: { S: owner },
                    id: { S: id },
                    productId: { S: productId },
                    inventoryProductId: { S: productId },
                    quantity: { N: String(item.quantity) },
                    color: { S: String(item.color) },
                    size: { S: String(item.size) },
                    weight: { N: String(item.weight) },
                    priceCents: { N: String(item.price_cents) },
                    salePriceCents: { N: String(item.sale_price_cents) },
                    costCents: { N: String(item.cost_cents) },
                    sku: { S: String(item.sku) },
                    length: { N: String(item.length) },
                    width: { N: String(item.width) },
                    height: { N: String(item.height) },
                    note: { S: String(item.note) },
                  },
                },
              };
            })
            .filter((v) => v),
        },
      };

      await asyncBatchWriteItem(params);
    }

    console.log("Inventory seeded!");
  } catch (e) {
    console.error("Failed to seed inventory: ", e.message);
  }
};
