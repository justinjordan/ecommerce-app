const uuidv5 = require("uuid").v5;
const csv = require("csvtojson");
const path = require("path");
const awsconfig = require("../awsconfig");
const { AWSDateTime } = require("../utils");

// for seeding UUID generation
const { userNamespace } = require("../uuid-namespaces");

module.exports = async function ({
  userTable,
  asyncCreateGroup,
  asyncGetUser,
  asyncCreateUser,
  asyncAddUserToGroup,
  asyncPut,
}) {
  const usernames = {};
  let errors = false;

  const userCsv = await csv().fromFile(
    path.join(__dirname, "../fixtures/users.csv")
  );

  try {
    await asyncCreateGroup({
      UserPoolId: awsconfig.aws_user_pools_id,
      GroupName: "superadmin",
    });
  } catch (e) {
    // fail silently
  }

  for (let user of userCsv) {
    try {
      // add user to Cognito
      let userData = await asyncGetUser({
        UserPoolId: awsconfig.aws_user_pools_id,
        Username: user.email,
      });

      if (!userData) {
        userData = await asyncCreateUser({
          UserPoolId: awsconfig.aws_user_pools_id,
          Username: user.email,
          TemporaryPassword: user.password_plain,
          UserAttributes: [
            {
              Name: "name",
              Value: user.name,
            },
            {
              Name: "email",
              Value: user.email,
            },
          ],
          MessageAction: "SUPPRESS",
        });
      }

      if (user.superadmin === "1") {
        asyncAddUserToGroup({
          UserPoolId: awsconfig.aws_user_pools_id,
          GroupName: "superadmin",
          Username: user.email,
        });
      }

      // add user to DynamoDB
      const userUUID = uuidv5(user.id, userNamespace);
      usernames[userUUID] = userData.Username; // store for other tables
      await asyncPut({
        TableName: userTable,
        Item: {
          __typename: "User", // graphql model name
          owner: userData.Username, // cognito username
          id: userUUID,
          name: String(user.name),
          email: String(user.email),
          superadmin: Boolean(user.superadmin),
          shopName: String(user.shop_name),
          rememberToken: String(user.remember_token),
          createdAt: AWSDateTime(user.created_at),
          updatedAt: AWSDateTime(user.updated_at),
          cardBrand: String(user.card_brand),
          cardLastFour: String(user.card_last_four),
          trialEndsAt: String(user.trial_ends_at),
          shopDomain: String(user.shop_domain),
          isEnabled: Boolean(user.is_enabled),
          billingPlan: String(user.billing_plan),
        },
      });
    } catch (e) {
      errors = true;
    }
  }

  if (errors) {
    console.error("Failed to seed producst");
  } else {
    console.log("Users seeded!");
  }

  return {
    usernames,
  };
};
