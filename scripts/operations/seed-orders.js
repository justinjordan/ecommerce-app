const uuidv5 = require("uuid").v5;
const csv = require("csvtojson");
const path = require("path");
const { chunkArray, AWSDateTime } = require("../utils");

// for seeding UUID generation
const { ordersNamespace, productNamespace } = require("../uuid-namespaces");

module.exports = async function ({
  asyncBatchWriteItem,
  orderTable,
  productOwners,
}) {
  const ordersCsv = await csv().fromFile(
    path.join(__dirname, "../fixtures/orders.csv")
  );

  // chunk into queries of 25 (DynamoDB limit)
  const chunkedOrdersCsv = chunkArray(ordersCsv, 25);

  try {
    for (let orders of chunkedOrdersCsv) {
      const params = {
        RequestItems: {
          [orderTable]: orders
            .map((item) => {
              const id = uuidv5(item.id, ordersNamespace);
              const productId = uuidv5(item.product_id, productNamespace);
              const owner = productOwners[productId];

              return {
                PutRequest: {
                  Item: {
                    __typename: { S: "Order" }, // graphql model name
                    owner: { S: owner },
                    id: { S: id },
                    productId: { S: item.product_id }, // product Int
                    orderProductId: { S: productId }, // product UUID
                    streetAddress: { S: String(item.street_address) },
                    apartment: { S: String(item.apartment) },
                    city: { S: String(item.city) },
                    state: { S: String(item.state) },
                    countryCode: { S: String(item.country_code) },
                    zip: { S: String(item.zip) },
                    phoneNumber: { S: String(item.phone_number) },
                    email: { S: String(item.email) },
                    name: { S: String(item.name) },
                    orderStatus: { S: String(item.order_status) },
                    paymentRef:
                      item.payment_ref === "NULL"
                        ? { NULL: true }
                        : { S: String(item.payment_ref) },
                    transactionId:
                      item.transaction_id === "NULL"
                        ? { NULL: true }
                        : { S: String(item.transaction_id) },
                    paymentAmtCents: { N: String(item.payment_amt_cents) },
                    shipChargedCents:
                      item.ship_charged_cents === "NULL"
                        ? { NULL: true }
                        : { S: String(item.ship_charged_cents) },
                    subtotalCents: { N: String(item.subtotal_cents) },
                    totalCents: { N: String(item.total_cents) },
                    shipperName: { S: String(item.shipper_name) },
                    paymentDate: { S: AWSDateTime(item.payment_date) },
                    shippedDate: { S: AWSDateTime(item.shipped_date) },
                    trackingNumber: { S: String(item.tracking_number) },
                    taxTotalCents: { N: String(item.tax_total_cents) },
                    createdAt: { S: AWSDateTime(item.created_at) },
                    updatedAt: { S: AWSDateTime(item.updated_at) },
                  },
                },
              };
            })
            .filter((v) => v),
        },
      };

      await asyncBatchWriteItem(params);
    }

    console.log("Orders seeded!");
  } catch (e) {
    console.error("Failed to seed orders: ", e.message);
  }
};
