const AWS = require("aws-sdk");
const localEnvInfo = require("../amplify/.config/local-env-info.json");
const localAwsInfo = require("../amplify/.config/local-aws-info.json");
const amplifyMeta = require("../amplify/backend/amplify-meta.json");
const seedUsers = require("./operations/seed-users");
const seedProducts = require("./operations/seed-products");
const seedInventory = require("./operations/seed-inventory");
const seedOrders = require("./operations/seed-orders");
const { promisify } = require("util");

const environmentName = localEnvInfo.envName;
const profileName = localAwsInfo[environmentName]?.profileName;

if (!profileName) {
  throw Error(
    "Please reinitialize your Amplify project using your AWS profile"
  );
}

(async () => {
  for (const apiName in amplifyMeta.api) {
    const providerName = amplifyMeta.api[apiName].providerPlugin;
    const region = amplifyMeta.providers[providerName].Region;

    AWS.config.credentials = new AWS.SharedIniFileCredentials({
      profile: profileName,
    });
    AWS.config.update({ region: region });
    const dynamodb = new AWS.DynamoDB();
    const documentClient = new AWS.DynamoDB.DocumentClient();
    const cognito = new AWS.CognitoIdentityServiceProvider();

    const asyncListTables = promisify(dynamodb.listTables).bind(dynamodb);
    const asyncCreateGroup = promisify(cognito.createGroup).bind(cognito);
    const asyncGetUser = promisify(cognito.adminGetUser).bind(cognito);
    const asyncCreateUser = promisify(cognito.adminCreateUser).bind(cognito);
    const asyncAddUserToGroup = promisify(cognito.adminAddUserToGroup).bind(
      cognito
    );
    const asyncPut = promisify(documentClient.put).bind(documentClient);
    const asyncBatchWriteItem = promisify(dynamodb.batchWriteItem).bind(
      dynamodb
    );

    try {
      const data = await asyncListTables({});
      const userTable = data.TableNames.find((table) => table.match("User"));
      const productTable = data.TableNames.find((table) =>
        table.match("Product")
      );
      const orderTable = data.TableNames.find((table) => table.match("Order"));
      const inventoryTable = data.TableNames.find((table) =>
        table.match("Inventory")
      );

      const { usernames } = await seedUsers({
        userTable,
        asyncCreateGroup,
        asyncGetUser,
        asyncCreateUser,
        asyncAddUserToGroup,
        asyncPut,
      });

      const { productOwners } = await seedProducts({
        asyncBatchWriteItem,
        usernames,
        productTable,
      });

      await seedInventory({
        asyncBatchWriteItem,
        inventoryTable,
        productOwners,
      });

      await seedOrders({
        asyncBatchWriteItem,
        orderTable,
        productOwners,
      });
    } catch (err) {
      console.error("Failed to seed database!", err.message);
    }
  }
})();
