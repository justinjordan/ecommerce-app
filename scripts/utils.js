module.exports = {
  chunkArray(arr, chunkSize) {
    return arr.reduce(
      (acc, cur) => {
        let currentChunk = acc[acc.length - 1];
        if (currentChunk.length >= chunkSize) {
          currentChunk = [];
          acc.push(currentChunk);
        }

        currentChunk.push(cur);

        return acc;
      },
      [[]]
    );
  },

  AWSDateTime(date) {
    date = date instanceof Date ? date : new Date(`${date} UTC`);
    return date.toISOString();
  },
};
