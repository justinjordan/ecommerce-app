const path = require("path");
const fs = require("fs");

module.exports = JSON.parse(
  fs
    .readFileSync(path.join(__dirname, "../src/aws-exports.js"), "utf8")
    .match(/= ({[\s\S]+});/)[1]
);
